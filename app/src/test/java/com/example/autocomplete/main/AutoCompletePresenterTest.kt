package com.example.autocomplete.main

import com.example.autocomplete.listener.AutoCompleteListener
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import mockit.Deencapsulation
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations

class AutoCompletePresenterTest {
    @Mock
    private lateinit var view: AutoCompleteInterface

    val  repository: AutoCompleteRepository = AutoCompleteRepository(mock())

    @Mock
    lateinit var listener: AutoCompleteListener

    lateinit var presenter: AutoCompletePresenter

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        presenter = AutoCompletePresenter(view, repository)
    }

    @Test
    fun testSetEndpoint() {
        val exendpoint = "name"

        presenter.setEndpoint(exendpoint)

        val endpoint = Deencapsulation.getField<String>(presenter, "endpoint")
        assertEquals(exendpoint, endpoint)
    }

    @Test
    fun testGetCountryApi() {
        val name = "thailand"
        val endpoint = "name"

        presenter.getCountryApi(name)

        verify(repository).getCountryName(endpoint, name, listener)
    }
}