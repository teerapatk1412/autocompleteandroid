package com.example.autocomplete.main

import com.example.autocomplete.api.AutoCompleteApiService
import com.example.autocomplete.listener.AutoCompleteListener
import com.example.autocomplete.model.CountryModel
import com.example.autocomplete.model.CurrencyModel
import com.example.autocomplete.model.LanguageModel
import com.nhaarman.mockitokotlin2.*
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AutoCompleteRepositoryTest {
    @Mock
    private lateinit var service: AutoCompleteApiService

    @Mock
    private lateinit var listener: AutoCompleteListener

    @InjectMocks
    private lateinit var repository: AutoCompleteRepository
    private lateinit var endpoint: String
    private lateinit var name: String

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        endpoint = "name"
        name = "thailand"
    }

    @Test
    fun testGetCountryApi() {
        whenever(service.getName(endpoint, name)).thenReturn(mock())

        repository.getCountryName(endpoint, name, listener)

        verify(service).getName(endpoint, name)

    }

    @Test
    fun testGetCountryApiResponseCountryModel() {
        val currencyModel = CurrencyModel("code","name","symbol")
        val languageModel = LanguageModel("iso1", "iso2", "name", "nativeName")
        val countryModel = CountryModel("thailand", "thailand", listOf(currencyModel), listOf(languageModel))
        val call = mock<Call<List<CountryModel>>>()
        whenever(service.getName(endpoint, name)).thenReturn(call)
        whenever(call.enqueue(any())).thenAnswer {
            it.getArgument<Callback<List<CountryModel>>>(0).onResponse(mock(), Response.success(listOf(countryModel)))
        }

        repository.getCountryName(endpoint, name, listener)

        verify(listener).setCountry(eq(listOf(countryModel.name)))

    }

    @Test
    fun testGetCountryApiFailed() {
        val call = mock<Call<List<CountryModel>>>()
        whenever(service.getName(endpoint, name)).thenReturn(call)
        whenever(call.enqueue(any())).thenAnswer {
            it.getArgument<Callback<List<CountryModel>>>(0).onFailure(mock(), mock())
        }

        repository.getCountryName(endpoint, name, listener)

        verify(listener).showErrorMessage(eq("Api Failed"))
    }

    @Test
    fun testGetCountryApiResponseBodyNull() {
        val call = mock<Call<List<CountryModel>>>()
        whenever(service.getName(endpoint, name)).thenReturn(call)
        whenever(call.enqueue(any())).thenAnswer {
            it.getArgument<Callback<List<CountryModel>>>(0).onResponse(mock(), Response.success(null))
        }

        repository.getCountryName(endpoint, name, listener)

        verifyZeroInteractions(listener)
    }

    @Test
    fun testListenerValueSuccess() {
        val captorListener: KArgumentCaptor<List<String>> = argumentCaptor()

        listener.setCountry(listOf(name))

        verify(listener).setCountry(captorListener.capture())
        assertEquals(listOf(name), captorListener.firstValue)
    }

    @Test
    fun testListenerValue() {
        val str = "Api Failed"
        val captorListener: KArgumentCaptor<String> = argumentCaptor()

        listener.showErrorMessage(str)

        verify(listener).showErrorMessage(captorListener.capture())
        assertEquals(str, captorListener.firstValue)
    }

}