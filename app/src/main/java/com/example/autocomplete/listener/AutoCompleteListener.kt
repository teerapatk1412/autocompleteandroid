package com.example.autocomplete.listener

interface AutoCompleteListener {
    fun setCountry(countryList: List<String?>)
    fun showErrorMessage(str: String)
}