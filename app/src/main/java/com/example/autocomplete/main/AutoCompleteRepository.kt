package com.example.autocomplete.main

import com.example.autocomplete.api.AutoCompleteApiService
import com.example.autocomplete.listener.AutoCompleteListener
import com.example.autocomplete.model.CountryModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

open class AutoCompleteRepository(private var service: AutoCompleteApiService) {

    fun getCountryName(endpoint: String, name: String, listener: AutoCompleteListener) {
        service.getName(endpoint, name).enqueue(object : Callback<List<CountryModel>> {
            override fun onFailure(call: Call<List<CountryModel>>, t: Throwable) {
                   listener.showErrorMessage("Api Failed")
            }

            override fun onResponse(
                call: Call<List<CountryModel>>,
                response: Response<List<CountryModel>>
            ) {
                response.body()?.apply {
                    listener.setCountry(this.map { it.name })
                }
            }
        })

    }
}