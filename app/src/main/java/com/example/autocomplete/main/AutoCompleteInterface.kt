package com.example.autocomplete.main

interface AutoCompleteInterface {
    fun setCountryModel(countyModel: List<String?>)
    fun showMessage(message: String)
}