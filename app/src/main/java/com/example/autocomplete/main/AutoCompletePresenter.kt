package com.example.autocomplete.main

import com.example.autocomplete.listener.AutoCompleteListener


class AutoCompletePresenter(val view: AutoCompleteInterface, private var repository: AutoCompleteRepository) {
    //    private var countryModel: List<CountryModel> = listOf()
    private var endpoint: String = ""

    fun getCountryApi(name: String) {
        val listener = object : AutoCompleteListener {
            override fun showErrorMessage(str: String) {
                   view.showMessage(str)
            }


            override fun setCountry(countryList: List<String?>) {
                if (countryList.isNotEmpty()) {
                    view.setCountryModel(countryList)
                }
            }
        }
        repository.getCountryName(endpoint, name, listener)

//        service.getName(endpoint, name).enqueue(object : Callback<List<CountryModel>> {
//            override fun onFailure(call: Call<List<CountryModel>>, t: Throwable) {
//                view.showMessage("Api Failed")
//            }
//
//            override fun onResponse(
//                call: Call<List<CountryModel>>,
//                response: Response<List<CountryModel>>
//            ) {
//                response.body()?.apply {
//                    if (this.isNotEmpty()) {
//                        countryModel = this
//                        view.setCountryModel(countryModel.map { it.name })
//                    }
//                }
//            }
//        })

    }

    fun setEndpoint(radioText: String) {
        endpoint = radioText
    }


}