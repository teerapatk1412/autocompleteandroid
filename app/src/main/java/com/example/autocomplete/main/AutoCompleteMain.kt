package com.example.autocomplete.main

import android.os.Bundle
import android.os.Handler
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.ArrayAdapter
import android.widget.RadioButton
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.autocomplete.R
import com.example.autocomplete.api.AutoCompleteApiManager
import kotlinx.android.synthetic.main.activity_main.*


class AutoCompleteMain : AppCompatActivity(), AutoCompleteInterface {

    private val presenter = AutoCompletePresenter(
        this,
        AutoCompleteRepository(AutoCompleteApiManager().createService())
    )
    lateinit var adapter: ArrayAdapter<String?>

    override fun setCountryModel(countyModel: List<String?>) {
        adapter = ArrayAdapter(this, android.R.layout.select_dialog_item, countyModel)
        acTextView.setAdapter(adapter)
        adapter.notifyDataSetChanged()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        acTextView.threshold = 1
        acTextView.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(p0: Editable?) {
                val handler = Handler()
                val runnable = Runnable {
                    presenter.getCountryApi(p0.toString())
                }
                handler.removeCallbacks(runnable)
                handler.postDelayed(runnable, 500)

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }
        })
    }

    override fun showMessage(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    fun radioButtonClick(view: View) {
        val radioButton: RadioButton = findViewById(rdGroup.checkedRadioButtonId)
        presenter.setEndpoint(radioButton.text.toString())
        acTextView.hint = getString(R.string.select_message, radioButton.text.toString())

    }

}
