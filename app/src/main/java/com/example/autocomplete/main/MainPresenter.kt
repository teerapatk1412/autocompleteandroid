package com.example.autocomplete.main

import com.example.autocomplete.api.AutoCompleteApiService
import com.example.autocomplete.model.CountyModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class MainPresenter(val view: MainInterface, private val service: AutoCompleteApiService) {

    private var countyModel: List<CountyModel> = listOf()
    private lateinit var endpoint: String

    fun getCountyApi(name: String) {
//        repository.getCallback(endpoint, name)
        service.getName(endpoint, name).enqueue(object : Callback<List<CountyModel>> {
            override fun onFailure(call: Call<List<CountyModel>>, t: Throwable) {
                view.showMessage("Api Failed")
            }

            override fun onResponse(
                call: Call<List<CountyModel>>,
                response: Response<List<CountyModel>>
            ) {
                response.body()?.apply {
                    if (this.isNotEmpty()) {
                        countyModel = this
                        view.setCountyModel(countyModel.map { it.name })
                    }
                }
            }
        })
    }

    fun setEndpoint(radioText: String) {
        endpoint = radioText
    }

}