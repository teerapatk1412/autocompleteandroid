package com.example.autocomplete.api

import com.example.autocomplete.model.CountryModel
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface AutoCompleteApiService {
    @GET("v2/{endpoint}/{name}")
    fun getName(@Path("endpoint") endpoint: String,@Path("name") name: String): Call<List<CountryModel>>

}