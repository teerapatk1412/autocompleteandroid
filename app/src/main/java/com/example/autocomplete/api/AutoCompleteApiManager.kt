package com.example.autocomplete.api

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class AutoCompleteApiManager {
    companion object {
       const val BASE_API = "https://restcountries.eu/rest/"
    }

    fun createService(): AutoCompleteApiService =
        Retrofit.Builder()
            .baseUrl(BASE_API)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .run { create(AutoCompleteApiService::class.java) }
}