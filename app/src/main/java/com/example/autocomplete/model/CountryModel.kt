package com.example.autocomplete.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CountryModel (
    @SerializedName("name")
    val name: String?,
    @SerializedName("capital")
    val capital: String?,
    @SerializedName("currencies")
    val currencies: List<CurrencyModel>,
    @SerializedName("languages")
    val languages: List<LanguageModel>
): Parcelable